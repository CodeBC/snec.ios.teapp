//
//  CataractViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 23/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "CataractViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CataractViewController ()

@end

@implementation CataractViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.supervisorTextField setInputAccessoryView:super.keyboardToolBar];
    [self.strengthsTextView setInputAccessoryView:super.keyboardToolBar];
    [self.weaknessTextView setInputAccessoryView:super.keyboardToolBar];
    [self.complicationsTextView setInputAccessoryView:super.keyboardToolBar];
    
    self.strengthsTextView.layer.cornerRadius = 5.0f;
    self.strengthsTextView.layer.masksToBounds = YES;
    self.strengthsTextView.layer.borderWidth = 0.5f;
    self.strengthsTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    self.weaknessTextView.layer.cornerRadius = 5.0f;
    self.weaknessTextView.layer.masksToBounds = YES;
    self.weaknessTextView.layer.borderWidth = 0.5f;
    self.weaknessTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    self.complicationsTextView.layer.cornerRadius = 5.0f;
    self.complicationsTextView.layer.masksToBounds = YES;
    self.complicationsTextView.layer.borderWidth = 0.5f;
    self.complicationsTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.complicationsTextView setPlaceholder:@"NA"];
    
    [self.supervisorTextField setText:self.supervisorName];
    
    // Arrange segmented control array by y coordinates
    self.feedbackSegmentedControls = [self.feedbackSegmentedControls sortedArrayUsingComparator:^NSComparisonResult(id view1, id view2) {
        if ([view1 frame].origin.y < [view2 frame].origin.y) return NSOrderedAscending;
        else if ([view1 frame].origin.y > [view2 frame].origin.y) return NSOrderedDescending;
        else return NSOrderedSame;
    }];
    
    self.formDate = [NSDate date];
    [self.dateLabel setText:[self.formDate ordinalString]];
    
    if (self.isReadOnly) {
        [self.contentView setUserInteractionEnabled:NO];
        [self.submitButton setHidden:YES];
    }
    [self.skipButton setHidden:YES];
    
    if (self.preloadData) {
        [self populateWithDataDict:self.preloadData];
    }
    
    
    if ([self.eventType isEqualToString:@"Phaco"]) {
        [self.surgerySegmentedControl setSelectedSegmentIndex:1];
    } else {
        [self.surgerySegmentedControl setSelectedSegmentIndex:0];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSupervisorTextField:nil];
    [self setStrengthsTextView:nil];
    [self setWeaknessTextView:nil];
    [self setComplicationsTextView:nil];
    [self setFeedbackSegmentedControls:nil];
    [self setDateLabel:nil];
    [self setSubmitButton:nil];
    [self setNumberOfCasesSegmentedControl:nil];
    [self setSkipButton:nil];
    [super viewDidUnload];
}

- (void)submitCataractForm:(NSMutableDictionary *)formParams {
    [SVProgressHUD showWithStatus:@"Submitting form..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *feedbackPath = [NSString stringWithFormat:@"/api/v1/cataract_surgeries?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] postPath:feedbackPath parameters:formParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Form Submitted!"];
                completed = YES;
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Form failed to submit. Please try again later."];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Form failed to submit. Please try again later."];
            NSLog(@"%@",[err localizedDescription]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Submission Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    [self resignKeyboard];
    [self dismissDatePicker:nil];
    
    if (self.supervisorTextField.text.length == 0 || self.strengthsTextView.text.length == 0 || self.weaknessTextView.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"Some fields not entered."];
        return;
    }
    
    NSDictionary *textParams = [self generateTextParams];
    
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] initWithDictionary:textParams];
    
    for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
        
        UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
        NSString *selectedTitle = [NSString stringWithFormat:@"%d",segmentedControl.selectedSegmentIndex+1];
        NSString *paramKey = [NSString stringWithFormat:@"cs[q%d]",i+1];
        [formParams setValue:selectedTitle forKey:paramKey];
        
    }
    
    [formParams setValue:[self.surgerySegmentedControl titleForSegmentAtIndex:self.surgerySegmentedControl.selectedSegmentIndex] forKey:@"cs[surgery]"];
    
    NSLog(@"Params: %@",formParams);
    
    [self submitCataractForm:formParams];
    
}

- (IBAction)skipButtonPressed:(id)sender {
    
    [self resignKeyboard];
    [self dismissDatePicker:nil];
    
    if (self.supervisorTextField.text.length == 0 || self.strengthsTextView.text.length == 0 || self.weaknessTextView.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"Some fields not entered."];
        return;
    }
    
    NSDictionary *textParams = [self generateTextParams];
    
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] initWithDictionary:textParams];
    
    for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
        
        NSString *paramKey = [NSString stringWithFormat:@"cs[q%d]",i+1];
        [formParams setValue:@"0" forKey:paramKey];
        
    }
    
    NSLog(@"Params: %@",formParams);
    
    [self submitCataractForm:formParams];
    
}

- (NSDictionary*)generateTextParams {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    return @{ @"cs[supervisor]": self.supervisorTextField.text,
       @"cs[cases]": [NSString stringWithFormat:@"%d",self.numberOfCasesSegmentedControl.selectedSegmentIndex+1],
       @"cs[strengths]": self.strengthsTextView.text,
       @"cs[weakness]": self.weaknessTextView.text,
       @"cs[complications]": self.complicationsTextView.text,
       @"cs[date]": [dateFormatter stringFromDate:self.formDate],
       @"cs[surgery]": [self.surgerySegmentedControl titleForSegmentAtIndex:self.surgerySegmentedControl.selectedSegmentIndex],
       @"cs[schedule_id]": [NSNumber numberWithInt:self.scheduleId] };
}

#pragma mark - Keyboard Handlers

- (void)handleKeyboardNotification:(NSNotification*)aNotification directionUp:(BOOL)up {
    
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    CGRect newFrame = self.scrollView.frame;
    newFrame.size.height -= (keyboardFrame.size.height) * (up ? 1 : -1);
    [self.scrollView setFrame:newFrame];
    
    [UIView commitAnimations];
}

- (void)resignKeyboard {
    [self.supervisorTextField resignFirstResponder];
    [self.strengthsTextView resignFirstResponder];
    [self.weaknessTextView resignFirstResponder];
    [self.complicationsTextView resignFirstResponder];
}

#pragma mark Pending Methods

- (NSDictionary *)generateDataDict {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDictionary *textParams = @{ @"form_name": @"Cataract Surgery Form",
                                  @"supervisor": self.supervisorTextField.text,
                                  @"cases": [NSString stringWithFormat:@"%d",self.numberOfCasesSegmentedControl.selectedSegmentIndex+1],
                                  @"strengths": self.strengthsTextView.text,
                                  @"weakness": self.weaknessTextView.text,
                                  @"complications": self.complicationsTextView.text,
                                  @"date": [dateFormatter stringFromDate:self.formDate],
                                  @"schedule_id": [NSNumber numberWithInt:self.scheduleId] };
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithDictionary:textParams];
    
    for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
        
        UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
        NSNumber *selectedTitle = [NSNumber numberWithInt:segmentedControl.selectedSegmentIndex+1];
        NSString *paramKey = [NSString stringWithFormat:@"q%d",i+1];
        [dataDict setValue:selectedTitle forKey:paramKey];
        
    }
    
    return dataDict;
}

- (void)populateWithDataDict:(NSDictionary *)dataDict {
    
    @try {
        
        self.scheduleId = [[dataDict objectForKey:@"schedule_id"] intValue];
        self.supervisorTextField.text = [dataDict objectForKey:@"supervisor"];
        self.numberOfCasesSegmentedControl.selectedSegmentIndex = [[dataDict objectForKey:@"cases"] intValue]-1;
        self.strengthsTextView.text = [dataDict objectForKey:@"strengths"];
        self.weaknessTextView.text = [dataDict objectForKey:@"weakness"];
        self.complicationsTextView.text = [dataDict objectForKey:@"complications"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        self.formDate = [dateFormatter dateFromString:[dataDict objectForKey:@"date"]];
        [self.dateLabel setText:[self.formDate ordinalString]];
        
        for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
            
            NSString *paramKey = [NSString stringWithFormat:@"q%d",i+1];
            
            UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
            
            if ([[dataDict objectForKey:paramKey] intValue] > 0) {
                [segmentedControl setSelectedSegmentIndex:[[dataDict objectForKey:paramKey] intValue]-1];
            }
            
        }
        
        if ([[dataDict objectForKey:@"surgery"] isEqualToString:@"ECCE"]) {
            [self.surgerySegmentedControl setSelectedSegmentIndex:0];
        } else {
            [self.surgerySegmentedControl setSelectedSegmentIndex:1];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Caught: %@",exception);
        [SVProgressHUD showErrorWithStatus:@"Form has errors"];
    }
}

@end
