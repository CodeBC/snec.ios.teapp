//
//  APIClient.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 20/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient

+ (APIClient *)sharedInstance {
    static APIClient *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    });
    
    return _sharedInstance;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
        self.parameterEncoding = AFFormURLParameterEncoding;
        [self setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    }
    
    return self;
}

@end
