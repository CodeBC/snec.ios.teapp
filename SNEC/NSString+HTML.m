//
//  NSString+HTML.m
//  SNEC
//
//  Created by Ye Myat Min on 8/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "NSString+HTML.h"


@implementation NSString (HTML)
-(NSString *) stringByStrippingHTML {
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
@end
