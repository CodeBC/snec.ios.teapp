//
//  EventInfoViewController.m
//  SNEC
//
//  Created by Ye Myat Min on 7/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "EventInfoViewController.h"
#import "NSString+HTML.h"
#import "QWebElement.h"

@implementation EventInfoViewController

- (void)viewDidLoad
{
    self.root = [[QRootElement alloc] init];
    self.root.title = [self.eventInfoDict objectForKey:@"title"];
    self.root.grouped = YES;
    
    
    QSection *descSection = [[QSection alloc] init];
    [descSection addElement:[[QTextElement alloc] initWithText: [[self.eventInfoDict objectForKey:@"description"] stringByStrippingHTML]]];
    [self.root addSection:descSection];
    
    QSection *section = [[QSection alloc] init];
    
    [section addElement:[[QLabelElement alloc] initWithTitle:@"Location" Value:[self.eventInfoDict objectForKey:@"location"]]];

    
    [section addElement:[[QLabelElement alloc] initWithTitle:@"Start" Value:[self.eventInfoDict objectForKey:@"start_at"]]];

    [section addElement:[[QLabelElement alloc] initWithTitle:@"End" Value:[self.eventInfoDict objectForKey:@"end_at"]]];

    
    if (![[self.eventInfoDict objectForKey:@"image"] isEqualToString:@""]) {
        [section addElement:[[QWebElement alloc] initWithTitle:@"E-poster" url:[self.eventInfoDict objectForKey:@"image"]]];
    }
    
    if (![[self.eventInfoDict objectForKey:@"pdf"] isEqualToString:@""]) {
        [section addElement:[[QWebElement alloc] initWithTitle:@"Article(s)" url:[self.eventInfoDict objectForKey:@"pdf"]]];
    }
    
    [self.root addSection:section];
    
    [super viewDidLoad];
}



@end
