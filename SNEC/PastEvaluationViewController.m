//
//  PastEvaluationViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 6/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "PastEvaluationViewController.h"
#import "CataractViewController.h"

@interface PastEvaluationViewController ()

@end

@implementation PastEvaluationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[[self.evaluationInfoDict objectForKey:@"trainees"] objectAtIndex:0] objectForKey:@"first_name"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.doneEvaluations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DoneEvaluationCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    
    // Configure the cell...
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *dateString = [[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"date"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"(dd MMM yyyy)"];
    NSString *stringFromDate = [dateFormatter stringFromDate:date];

    
    NSLog(@"%@", date);
    
    if (dateString.length == 0) {
        cell.textLabel.text = [[[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"supervisor"] stringByAppendingString:[[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"schedule_type_name"]];
        cell.detailTextLabel.text = @"(No Date)";
    } else {
        if([[self.doneEvaluations objectAtIndex:indexPath.row] containsObject:@"schedule_type_name"]) {
            cell.textLabel.text = [[[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"supervisor"] stringByAppendingString:[[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"schedule_type_name"]];
        } else {
            cell.textLabel.text = [[self.doneEvaluations objectAtIndex:indexPath.row] objectForKey:@"supervisor"];
        }

        cell.detailTextLabel.text = stringFromDate;
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];

    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     
     if (self.evaluationInfoDict) {
         if ([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_CATARACT) {
         
             CataractViewController *cataractView = [self.storyboard instantiateViewControllerWithIdentifier:@"CataractForm"];
             cataractView.isReadOnly = YES;
             cataractView.preloadData = [self.doneEvaluations objectAtIndex:indexPath.row];
             
             [self.navigationController pushViewController:cataractView animated:YES];
             
         }
     }
}

@end
