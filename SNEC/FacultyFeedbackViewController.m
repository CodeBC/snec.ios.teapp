//
//  FacultyFeedbackViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FacultyFeedbackViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface FacultyFeedbackViewController ()

@end

@implementation FacultyFeedbackViewController

@synthesize selectedRow;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.commentsTextView setInputAccessoryView:super.keyboardToolBar];
    
    self.commentsTextView.layer.cornerRadius = 5.0f;
    self.commentsTextView.layer.masksToBounds = YES;
    self.commentsTextView.layer.borderWidth = 0.5f;
    self.commentsTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    if (self.isReadOnly) {
        [self.contentView setUserInteractionEnabled:NO];
        [self.submitButton setHidden:YES];
    }
    
    if (self.preloadData) {
        [self populateWithDataDict:self.preloadData];
    }
    
    self.residentArray = @[];
    
    [SVProgressHUD showWithStatus:@"Fetching data..." maskType:SVProgressHUDMaskTypeGradient];
    NSString *residentPath = [NSString stringWithFormat:@"/api/v1/users/trainee?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] getPath:residentPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
    
        
        NSError *err;
        /*
         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
         
         if ([responseDict objectForKey:@"data"]) {
         NSLog(@"%@",responseDict);
         self.residentArray = [responseDict objectForKey:@"data"];
         }
         */
        NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseArray) {
            self.residentArray = responseArray;
        }
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Error fetching trainees. Please try again."];
    }];
    
    NSString *facultyPath = [NSString stringWithFormat:@"/api/v1/users/supervisors?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] getPath:facultyPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSError *err;
        NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseArray) {
            self.facultyArray = responseArray;
        }
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Error fetching faculties. Please try again."];
    }];
    
    NSNumber *userType = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_TYPE_KEY];
    if([userType intValue] == kUserTypeTrainee) {
        NSString *name = [NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_FIRST_NAME_KEY], [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_LAST_NAME_KEY]];
        [self.traineeTextField setTitle:name forState:UIControlStateNormal];
        [self.traineeTextField setEnabled:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFacultyTextField:nil];
    [self setTraineeTextField:nil];
    [self setCommentsTextView:nil];
    [self setDateButton:nil];
    [self setFeedbackTypeSegmentedControl:nil];
    [self setSubmitButton:nil];
    [super viewDidUnload];
}

- (IBAction)dateButtonPressed:(id)sender {
    [self showDatePicker:nil];
}

- (void)dismissDatePicker:(id)sender {
    [super dismissDatePicker:sender];
    
    if (self.datePicker) {
        self.formDate = self.datePicker.date;
        [self.dateButton setTitle:[self.formDate ordinalString] forState:UIControlStateNormal];
    }
}

- (IBAction)submitButtonPressed:(id)sender {
    
    [self resignKeyboard];
    [self dismissDatePicker:nil];
    
    if (self.facultyTextField.titleLabel.text.length == 0 || self.traineeTextField.titleLabel.text.length == 0 || self.commentsTextView.text.length == 0 || [self.dateButton.titleLabel.text isEqualToString:@"Date of Activity"]) {
        [SVProgressHUD showErrorWithStatus:@"Some fields not entered."];
        return;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDictionary *feedbackParams = @{@"feedback[faculty_name]": self.facultyTextField.titleLabel.text, @"feedback[trainee_name]": self.traineeTextField.titleLabel.text, @"feedback[feedback_type]": [self.feedbackTypeSegmentedControl titleForSegmentAtIndex:self.feedbackTypeSegmentedControl.selectedSegmentIndex], @"feedback[comments]": self.commentsTextView.text, @"feedback[date_of_activity]": [dateFormatter stringFromDate:self.formDate] };
    
    [SVProgressHUD showWithStatus:@"Submitting feedback..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *feedbackPath = [NSString stringWithFormat:@"/api/v1/feedbacks?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] postPath:feedbackPath parameters:feedbackParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Feedback Submitted!"];
                completed = YES;
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Feedback failed to submit. Please try again later."];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Feedback failed to submit. Please try again later."];
            NSLog(@"%@",[err localizedDescription]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Submission Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];

    
}

#pragma mark - Keyboard Handlers

- (void)handleKeyboardNotification:(NSNotification*)aNotification directionUp:(BOOL)up {
    
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    CGRect newFrame = self.scrollView.frame;
    newFrame.size.height -= (keyboardFrame.size.height) * (up ? 1 : -1);
    [self.scrollView setFrame:newFrame];
    
    [UIView commitAnimations];
}

- (void)resignKeyboard {
    [self.facultyTextField resignFirstResponder];
    [self.traineeTextField resignFirstResponder];
    [self.commentsTextView resignFirstResponder];
}

#pragma mark Pending Methods 

- (NSDictionary *)generateDataDict {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    return @{@"form_name": @"Faculty Feedback Form",
             @"faculty_name": self.facultyTextField.titleLabel.text,
             @"trainee_name": self.traineeTextField.titleLabel.text,
             @"feedback_type": [self.feedbackTypeSegmentedControl titleForSegmentAtIndex:self.feedbackTypeSegmentedControl.selectedSegmentIndex],
             @"comments": self.commentsTextView.text,
             @"date_of_activity": [dateFormatter stringFromDate:self.formDate]
             };
    
}

- (void)populateWithDataDict:(NSDictionary *)dataDict {
    
    @try {
        
        [self.facultyTextField setTitle:[dataDict objectForKey:@"faculty_name"] forState:UIControlStateNormal];
        [self.traineeTextField setTitle:[dataDict objectForKey:@"trainee_name"] forState:UIControlStateNormal];
        self.commentsTextView.text = [dataDict objectForKey:@"comments"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.formDate = [dateFormatter dateFromString:[dataDict objectForKey:@"date_of_activity"]];
        [self.dateButton setTitle:[self.formDate ordinalString] forState:UIControlStateNormal];
        
        if ([[dataDict objectForKey:@"feedback_type"] isEqualToString:@"Improvement"]) {
            [self.feedbackTypeSegmentedControl setSelectedSegmentIndex:0];
        } else {
            [self.feedbackTypeSegmentedControl setSelectedSegmentIndex:1];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Caught: %@",exception);
        [SVProgressHUD showErrorWithStatus:@"Form has errors"];
    }
    
}

#pragma mark - Picker Data/Delegate

- (void)removeViews:(id)object {
    [[self.view viewWithTag:19] removeFromSuperview];
    [[self.view viewWithTag:999] removeFromSuperview];
    [[self.view viewWithTag:777] removeFromSuperview];
    [[self.view viewWithTag:111] removeFromSuperview];
}

- (void)dismissPicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:19].alpha = 0;
    [self.view viewWithTag:999].frame = datePickerTargetFrame;
    [self.view viewWithTag:777].frame = datePickerTargetFrame;
    [self.view viewWithTag:111].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

- (IBAction)displayPicker:(id)sender andTag:(int) tag {
    if ([self.view viewWithTag:19]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 19;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    pickerView.tag = tag;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
    pickerView.backgroundColor = [UIColor whiteColor];
    [pickerView selectRow:selectedRow inComponent:0 animated:NO];
    [self.view addSubview:pickerView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 111;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPicker:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    pickerView.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
}

- (void) traineeButtonPressed:(id)sender {
    [self displayPicker:nil andTag:999];
}

- (void) facultyButtonPressed:(id)sender {
    [self displayPicker:nil andTag:777];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if(thePickerView.tag == 777) {
        return [self.facultyArray count];
    }
    return [self.residentArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(thePickerView.tag == 777) {
        return [NSString stringWithFormat:@"%@ %@",[[self.facultyArray objectAtIndex:row] objectForKey:@"first_name"],[[self.facultyArray objectAtIndex:row] objectForKey:@"last_name"]];
    }
    return [NSString stringWithFormat:@"%@ %@",[[self.residentArray objectAtIndex:row] objectForKey:@"first_name"],[[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    if (thePickerView.tag == 999) {
        NSString *title = [NSString stringWithFormat:@"%@ %@", [[self.residentArray objectAtIndex:row] objectForKey:@"first_name"], [[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
        [self.traineeTextField setTitle:title forState:UIControlStateNormal];
    } else {
        NSString *title = [NSString stringWithFormat:@"%@ %@", [[self.facultyArray objectAtIndex:row] objectForKey:@"first_name"], [[self.facultyArray objectAtIndex:row] objectForKey:@"last_name"]];
        [self.facultyTextField setTitle:title forState:UIControlStateNormal];
    }

}


@end
