//
//  SettingsViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "SettingsViewController.h"
#import "LoginViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.usernameLabel setText:[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_EMAIL_KEY]];
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width,self.scrollView.frame.size.height-49.0f)];
    
    UIToolbar *keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboardPressed:)];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
    [keyboardToolBar setItems:itemsArray];
    
    
    [self.passwordTextField setInputAccessoryView:keyboardToolBar];
    [self.confirmPasswordTextField setInputAccessoryView:keyboardToolBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resignKeyboardPressed:(id)sender {
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}

- (IBAction)changePasswordButtonPressed:(id)sender {
    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text] && ![self.passwordTextField.text isEqualToString:@""]) {
        
        [self resignKeyboardPressed:nil];
        
        NSDictionary *loginParams = @{@"auth_token": [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY], @"user_login[password]": self.passwordTextField.text };
        
        [SVProgressHUD showWithStatus:@"Changing password..." maskType:SVProgressHUDMaskTypeGradient];
        [[APIClient sharedInstance] putPath:@"/api/users/password" parameters:loginParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
            
            NSLog(@"%@",responseString);
            
            NSError *err;
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
            
            if (responseDict) {
                NSLog(@"%@",responseDict);
                
                [SVProgressHUD showSuccessWithStatus:@"Password Changed!"];
                [[NSUserDefaults standardUserDefaults] setObject:[responseDict objectForKey:@"auth_token"] forKey:DEFAULTS_AUTH_TOKEN_KEY];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Password change error. Please try again."];
                NSLog(@"%@",[err localizedDescription]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Change Password Error: %@",[error localizedDescription]);
            [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
        }];
        
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"Passwords do not match."];
    }
}

- (IBAction)logoutButtonPressed:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:DEFAULTS_AUTH_TOKEN_KEY];
    
    LoginViewController *loginView = [self.storyboard instantiateInitialViewController];
    loginView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:loginView animated:YES];
}

- (void)viewDidUnload {
    [self setUsernameLabel:nil];
    [self setPasswordTextField:nil];
    [self setConfirmPasswordTextField:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    [self moveTextViewForKeyboard:aNotification up:YES];
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
    [self moveTextViewForKeyboard:aNotification up:NO];
}

- (void)moveTextViewForKeyboard:(NSNotification*)aNotification up:(BOOL)up {
    
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    CGRect newFrame = self.scrollView.frame;
    newFrame.size.height -= (keyboardFrame.size.height-49.0f) * (up ? 1 : -1);
    [self.scrollView setFrame:newFrame];
    
    [UIView commitAnimations];
}
@end
