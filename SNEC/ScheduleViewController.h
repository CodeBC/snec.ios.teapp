//
//  ScheduleViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TapkuLibrary/TapkuLibrary.h>

/* 
 
 Ensure Build Settings for SNEC Project has "Header Search Paths" pointing
 to the "Vendor" folder. 
 
 */

@interface ScheduleViewController : TKCalendarMonthTableViewController

- (IBAction)refreshButtonPressed:(id)sender;

@end
