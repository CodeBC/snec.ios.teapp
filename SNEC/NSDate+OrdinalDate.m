//
//  NSDate+OrdinalDate.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 26/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "NSDate+OrdinalDate.h"
#import "TTTOrdinalNumberFormatter.h"

@implementation NSDate (OrdinalDate)

- (NSString *)ordinalString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComps = [calendar components:NSDayCalendarUnit fromDate:self];
    
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    [ordinalNumberFormatter setLocale:[NSLocale currentLocale]];
    [ordinalNumberFormatter setGrammaticalGender:TTTOrdinalNumberFormatterMaleGender];
    NSNumber *dayNumber = [NSNumber numberWithInteger:[dateComps day]];
    [dateFormatter setDateFormat:@"MMM yyyy"];
    
    NSString *ordinalString = [NSString stringWithFormat:@"%@ %@",[ordinalNumberFormatter stringFromNumber:dayNumber],[dateFormatter stringFromDate:self] ];
    
    return ordinalString;
}

@end
