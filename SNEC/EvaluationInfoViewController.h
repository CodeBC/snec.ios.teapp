//
//  EvaluationInfoViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvaluationInfoViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
    NSInteger selectedRow;
}

@property (strong, nonatomic) NSDictionary *evaluationInfoDict;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UILabel *supervisorLabel;
@property (strong, nonatomic) IBOutlet UILabel *traineeLabel;
@property (strong, nonatomic) IBOutlet UILabel *supervisorHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *traineeHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *scheduleTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *scheduleTypeHeaderLabel;

@property (strong, nonatomic) IBOutlet UIButton *evaluationButton;
@property (strong, nonatomic) IBOutlet UIButton *pastEvaluationButton;

@property (strong, nonatomic) IBOutlet UIButton *supervisorButton;
@property (strong, nonatomic) IBOutlet UIButton *traineeButton;



- (IBAction)evaluationButtonPressed:(id)sender;
- (IBAction)pastEvaluationButtonPressed:(id)sender;
- (IBAction)traineeButtonPressed:(id)sender;

@end
