//
//  FeedbackFormsViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 21/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FeedbackFormsViewController.h"
#import "FacultyFeedbackViewController.h"

@interface FeedbackFormsViewController ()

@property (nonatomic, strong) NSArray *formsArray;

@end

@implementation FeedbackFormsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSNumber *userType = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_TYPE_KEY];
    
    if([userType intValue] == kUserTypeTrainee) {
        self.formsArray = @[ @"Early Concern Card", @"Praise Card", @"Feedback Form on Faculty", @"Grand Round Presentation" ];
    } else {
        self.formsArray = @[ @"Early Concern Card", @"Praise Card", @"Grand Round Presentation" ];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.formsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FormCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [cell.textLabel setText:[self.formsArray objectAtIndex:indexPath.row]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIViewController *formController;
    
    if ([self.formsArray count] == 4) {
        switch (indexPath.row) {
                
            case 0:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"EarlyConcernForm"];
                break;
                
            case 1:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"PraiseCardForm"];
                break;
                
            case 2:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"FacultyFeedbackForm"];
                break;
                
            case 3:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"GRPView"];
                break;
                
            default:
                break;
        }
    } else {
        switch (indexPath.row) {
                
            case 0:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"EarlyConcernForm"];
                break;
                
            case 1:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"PraiseCardForm"];
                break;
                
            case 2:
                formController = [self.storyboard instantiateViewControllerWithIdentifier:@"GRPView"];
                break;
                
            default:
                break;
        }
    }
    
    
    if (formController) {
        [self.navigationController pushViewController:formController animated:YES];
    }
}

@end
