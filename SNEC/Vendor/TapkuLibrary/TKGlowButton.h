//
//  TKGlowButton.h
//  TapkuLibrary
//
//  Created by Devin Ross on 8/24/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface TKGlowButton : UIButton


- (void) setBackgroundColor:(UIColor*)color forState:(UIControlState)state;

@end
