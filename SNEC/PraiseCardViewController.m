//
//  PraiseCardViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "PraiseCardViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PraiseCardViewController ()

@end

@implementation PraiseCardViewController

@synthesize selectedRow;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.evaluatorTextField setInputAccessoryView:super.keyboardToolBar];
    [self.commentsTextView setInputAccessoryView:super.keyboardToolBar];
    
    self.commentsTextView.layer.cornerRadius = 5.0f;
    self.commentsTextView.layer.masksToBounds = YES;
    self.commentsTextView.layer.borderWidth = 0.5f;
    self.commentsTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    selectedRow = 0;
    
    if (self.isReadOnly) {
        [self.contentView setUserInteractionEnabled:NO];
        [self.submitButton setHidden:YES];
    }
    
    if (self.preloadData) {
        [self populateWithDataDict:self.preloadData];
    }
    
    NSString *evaluatorName = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_FIRST_NAME_KEY];
    if (evaluatorName) {
        NSString *name = [NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_FIRST_NAME_KEY], [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_LAST_NAME_KEY]];
        [self.evaluatorTextField setText: name];
        [self.evaluatorTextField setEnabled:NO];
    }
    
    self.residentArray = @[];
    
    [SVProgressHUD showWithStatus:@"Fetching residents..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *residentPath = [NSString stringWithFormat:@"/api/v1/users/trainee?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    
    [[APIClient sharedInstance] getPath:residentPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        /*
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if ([responseDict objectForKey:@"data"]) {
            NSLog(@"%@",responseDict);
            self.residentArray = [responseDict objectForKey:@"data"];
        }
         */
        NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseArray) {
            NSLog(@"%@",responseArray);
            self.residentArray = responseArray;
        }
        
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Error fetching residents. Please try again."];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setEvaluatorTextField:nil];
    [self setCommentsTextView:nil];
    [self setAltruismCheckBox:nil];
    [self setHonestyCheckBox:nil];
    [self setCaringCheckBox:nil];
    [self setRespectOthersCheckBox:nil];
    [self setRespectDifferencesCheckBox:nil];
    [self setResponsibilityCheckBox:nil];
    [self setExcellenceCheckBox:nil];
    [self setLeadershipCheckBox:nil];
    [self setKnowledgeCheckBox:nil];
    [self setDateButton:nil];
    [self setSubmitButton:nil];
    [self setResidentButton:nil];
    [super viewDidUnload];
}

- (void)dismissDatePicker:(id)sender {
    [super dismissDatePicker:sender];
    
    if (super.datePicker) {
        self.formDate = self.datePicker.date;
        [self.dateButton setTitle:[self.formDate ordinalString] forState:UIControlStateNormal];
    }
}

- (IBAction)residentButtonPressed:(id)sender {
    [self displayPicker:nil];
}

- (IBAction)dateButtonPressed:(id)sender {
    [self showDatePicker:nil];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    [self resignKeyboard];
    [self dismissDatePicker:nil];
    [self dismissPicker:nil];
    
    if ([self.residentButton.titleLabel.text isEqualToString:@"Choose Resident"] || self.evaluatorTextField.text.length == 0 || self.commentsTextView.text.length == 0 || [self.dateButton.titleLabel.text isEqualToString:@"Date of Activity"]) {
        [SVProgressHUD showErrorWithStatus:@"Some fields not entered."];
        return;
    }
    
    NSString *basedOnString = [self generateBasedOnString];
    NSLog(@"%@",basedOnString);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDictionary *feedbackParams = @{@"pc[resident]": [self.residentButton titleForState:UIControlStateNormal], @"pc[evaluator]": self.evaluatorTextField.text, @"pc[comments]": self.commentsTextView.text, @"pc[doa]": [dateFormatter stringFromDate:self.formDate], @"pc[based_on]": basedOnString };
    
    [SVProgressHUD showWithStatus:@"Submitting feedback..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *feedbackPath = [NSString stringWithFormat:@"/api/v1/praise_cards?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] postPath:feedbackPath parameters:feedbackParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Feedback Submitted!"];
                completed = YES;
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Feedback failed to submit. Please try again later."];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Feedback failed to submit. Please try again later."];
            NSLog(@"%@",[err localizedDescription]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Submission Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
    
}

- (NSString *)generateBasedOnString {
    NSMutableArray *resultArray = [NSMutableArray array];
    
    if ([self.altruismCheckBox isChecked]) [resultArray addObject:@"altruism"];
    if ([self.honestyCheckBox isChecked]) [resultArray addObject:@"honesty"];
    if ([self.caringCheckBox isChecked]) [resultArray addObject:@"caring"];
    if ([self.respectOthersCheckBox isChecked]) [resultArray addObject:@"respectOthers"];
    if ([self.respectDifferencesCheckBox isChecked]) [resultArray addObject:@"respectDifferences"];
    if ([self.responsibilityCheckBox isChecked]) [resultArray addObject:@"responsibility"];
    if ([self.excellenceCheckBox isChecked]) [resultArray addObject:@"excellence"];
    if ([self.leadershipCheckBox isChecked]) [resultArray addObject:@"leadership"];
    if ([self.knowledgeCheckBox isChecked]) [resultArray addObject:@"knowledge"];
    
    return [resultArray componentsJoinedByString:@","];
}

#pragma mark - Keyboard Handlers

- (void)handleKeyboardNotification:(NSNotification*)aNotification directionUp:(BOOL)up {
    
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    CGRect newFrame = self.scrollView.frame;
    newFrame.size.height -= (keyboardFrame.size.height) * (up ? 1 : -1);
    [self.scrollView setFrame:newFrame];
    
    [UIView commitAnimations];
}

- (void)resignKeyboard {
    [self.evaluatorTextField resignFirstResponder];
    [self.commentsTextView resignFirstResponder];
}

#pragma mark Pending Methods

- (NSDictionary *)generateDataDict {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    return @{@"form_name": @"Praise Card Form",
             @"resident": [self.residentButton titleForState:UIControlStateNormal],
             @"evaluator": self.evaluatorTextField.text,
             @"comments": self.commentsTextView.text,
             @"doa": [dateFormatter stringFromDate:self.formDate],
             @"based_on": [self generateBasedOnString] };
    
}

- (void)populateWithDataDict:(NSDictionary *)dataDict {
    
    @try {
        
        self.evaluatorTextField.text = [dataDict objectForKey:@"evaluator"];
        self.commentsTextView.text = [dataDict objectForKey:@"comments"];
        [self.residentButton setTitle:[dataDict objectForKey:@"resident"] forState:UIControlStateNormal];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        self.formDate = [dateFormatter dateFromString:[dataDict objectForKey:@"doa"]];
        [self.dateButton setTitle:[self.formDate ordinalString] forState:UIControlStateNormal];
        
        NSArray *basedOnArray = [[dataDict objectForKey:@"based_on"] componentsSeparatedByString:@","];
        
        for (NSString *checkString in basedOnArray) {
            
            if ([checkString isEqualToString:@"altruism"]) [self.altruismCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"honesty"]) [self.honestyCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"caring"]) [self.caringCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"respectOthers"]) [self.respectOthersCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"respectDifferences"]) [self.respectDifferencesCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"responsibility"]) [self.responsibilityCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"excellence"]) [self.excellenceCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"leadership"]) [self.leadershipCheckBox setChecked:YES];
            if ([checkString isEqualToString:@"knowledge"]) [self.knowledgeCheckBox setChecked:YES];
            
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Caught: %@",exception);
        [SVProgressHUD showErrorWithStatus:@"Form has errors"];
    }
    
}

#pragma mark - Picker Data/Delegate

- (void)removeViews:(id)object {
    [[self.view viewWithTag:19] removeFromSuperview];
    [[self.view viewWithTag:110] removeFromSuperview];
    [[self.view viewWithTag:111] removeFromSuperview];
}

- (void)dismissPicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:19].alpha = 0;
    [self.view viewWithTag:110].frame = datePickerTargetFrame;
    [self.view viewWithTag:111].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

- (IBAction)displayPicker:(id)sender {
    if ([self.view viewWithTag:19]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 19;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    pickerView.tag = 110;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
    pickerView.backgroundColor = [UIColor whiteColor];
    [pickerView selectRow:selectedRow inComponent:0 animated:NO];
    [self.view addSubview:pickerView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 111;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPicker:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    pickerView.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.residentArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@ %@",[[self.residentArray objectAtIndex:row] objectForKey:@"first_name"],[[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString *title = [NSString stringWithFormat:@"%@ %@", [[self.residentArray objectAtIndex:row] objectForKey:@"first_name"], [[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
    [self.residentButton setTitle:title forState:UIControlStateNormal];
    selectedRow = row;
}


@end
