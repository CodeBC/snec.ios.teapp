//
//  LoginViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {

    [self resignKeyboardButton:nil];

    NSDictionary *loginParams = @{@"user_login[email]": self.usernameTextField.text, @"user_login[password]": self.passwordTextField.text };

    [SVProgressHUD showWithStatus:@"Signing in..." maskType:SVProgressHUDMaskTypeGradient];
    [[APIClient sharedInstance] postPath:@"/api/users/sign_in" parameters:loginParams success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];

        NSLog(@"%@",responseString);

        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];

        if (responseDict) {
            NSLog(@"%@",responseDict);

            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Successfully signed in!"];
                [self saveUserInfo:responseDict];
                [self performSegueWithIdentifier:@"kLoadTabBarView" sender:self];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Please check your credentials and try again."];
            }

        } else {
            [SVProgressHUD showErrorWithStatus:@"Please check your credentials and try again."];
            NSLog(@"%@",[err localizedDescription]);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Login Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
}

- (IBAction)resignKeyboardButton:(id)sender {
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if ([self.usernameTextField isFirstResponder]) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [self loginButtonPressed:nil];
    }

    return NO;
}

- (void)saveUserInfo:(NSDictionary*)loginResponse {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[loginResponse objectForKey:@"auth_token"] forKey:DEFAULTS_AUTH_TOKEN_KEY];
    [defaults setValue:[loginResponse objectForKey:@"email"] forKey:DEFAULTS_USER_EMAIL_KEY];
    [defaults setValue:[loginResponse objectForKey:@"user_type"] forKey:DEFAULTS_USER_TYPE_KEY];
    [defaults setValue:[loginResponse objectForKey:@"first_name"] forKey:DEFAULTS_USER_FIRST_NAME_KEY];
    [defaults setValue:[loginResponse objectForKey:@"last_name"] forKey:DEFAULTS_USER_LAST_NAME_KEY];

    if (self.rememberSwitch.on) {
        [defaults setBool:YES forKey:DEFAULTS_USER_REMEMBER_KEY];
    } else {
        [defaults setBool:NO forKey:DEFAULTS_USER_REMEMBER_KEY];
    }
}

- (void)viewDidUnload {
    [self setUsernameTextField:nil];
    [self setPasswordTextField:nil];
    [self setRememberSwitch:nil];
    [self setLoginButton:nil];
    [super viewDidUnload];
}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    [self moveTextViewForKeyboard:aNotification up:YES];
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
    [self moveTextViewForKeyboard:aNotification up:NO];
}

- (void)moveTextViewForKeyboard:(NSNotification*)aNotification up:(BOOL)up {

    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;

    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];

    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];

    [self.view setFrame:CGRectOffset(self.view.frame, 0, -keyboardFrame.size.height * (up ? 1 : -1) )];

    [UIView commitAnimations];
}

@end
