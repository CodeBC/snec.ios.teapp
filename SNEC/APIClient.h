//
//  APIClient.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 20/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface APIClient : AFHTTPClient

+ (APIClient *)sharedInstance;

@end
