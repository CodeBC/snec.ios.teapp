//
//  NSDate+OrdinalDate.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 26/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (OrdinalDate)

- (NSString *)ordinalString;

@end
