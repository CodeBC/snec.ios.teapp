//
//  FacultyFeedbackViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormBaseViewController.h"

@interface FacultyFeedbackViewController : FormBaseViewController <UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) NSArray *residentArray;
@property (strong, nonatomic) NSArray *facultyArray;
@property (assign, nonatomic) NSInteger selectedRow;

@property (strong, nonatomic) IBOutlet UIButton *facultyTextField;
@property (strong, nonatomic) IBOutlet UIButton *traineeTextField;
@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;
@property (strong, nonatomic) IBOutlet UIButton *dateButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *feedbackTypeSegmentedControl;


@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)dateButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;

- (IBAction)facultyButtonPressed:(id)sender;
- (IBAction)traineeButtonPressed:(id)sender;

@end
