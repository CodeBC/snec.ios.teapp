//
//  PraiseCardViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FormBaseViewController.h"
#import "UICheckBox.h"

@interface PraiseCardViewController : FormBaseViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *evaluatorTextField;
@property (strong, nonatomic) IBOutlet UIButton *dateButton;
@property (strong, nonatomic) IBOutlet UIButton *residentButton;

@property (strong, nonatomic) NSArray *residentArray;
@property (assign, nonatomic) NSInteger selectedRow;

@property (strong, nonatomic) IBOutlet UICheckBox *altruismCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *honestyCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *caringCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *respectOthersCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *respectDifferencesCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *responsibilityCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *excellenceCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *leadershipCheckBox;
@property (strong, nonatomic) IBOutlet UICheckBox *knowledgeCheckBox;

@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)residentButtonPressed:(id)sender;
- (IBAction)dateButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;

@end
