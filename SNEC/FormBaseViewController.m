//
//  FormBaseViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FormBaseViewController.h"

@interface FormBaseViewController ()

@end

@implementation FormBaseViewController

@synthesize datePicker;
@synthesize keyboardToolBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.scrollView setContentSize:self.contentView.frame.size];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    keyboardToolBar = [[UIToolbar alloc] init];
    [keyboardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [keyboardToolBar sizeToFit];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard)];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
    [keyboardToolBar setItems:itemsArray];

}

- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"Posted notification");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REFRESH_SCHEDULE"
                                                        object:nil];
    if (completed && self.isEditingExistingForm) {
        [self removeFromPendingArray];
    }
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setContentView:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Date Picker

- (void)removeDatePickerViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender {
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeDatePickerViews:)];
    [UIView commitAnimations];
}

- (void)showDatePicker:(id)sender {
    if ([self.view viewWithTag:9]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    datePicker.tag = 10;
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
    datePicker.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissDatePicker:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
}

#pragma mark - Keyboard Handling

- (void)resignKeyboard {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    [self handleKeyboardNotification:aNotification directionUp:YES];
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
    [self handleKeyboardNotification:aNotification directionUp:NO];
}

- (void)handleKeyboardNotification:(NSNotification*)aNotification directionUp:(BOOL)up {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

#pragma Back Button

- (IBAction)backButtonPressed:(id)sender {
    
    if (self.isReadOnly) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save Form?" message:@"Your incomplete forms will be saved in My Evaluations Tab." delegate:self cancelButtonTitle:@"Discard" otherButtonTitles:@"Save",nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        [self updatePendingArray];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Data Saving/Loading

- (void)updatePendingArray {
    
    NSArray *fileArray = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* filePath = [[fileArray objectAtIndex:0] stringByAppendingPathComponent:@"/pending.plist"];
    NSLog(@"File Path: %@", filePath);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    NSMutableArray *pendingArray;
    if (fileExists) {
        pendingArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    } else {
        pendingArray = [[NSMutableArray alloc] init];
    }
    
    if (self.isEditingExistingForm) {
        [pendingArray replaceObjectAtIndex:self.editingIndex withObject:[self generateDataDict]];
    } else {
        [pendingArray insertObject:[self generateDataDict] atIndex:0];
    }
    NSLog(@"Pending Array: %@",pendingArray);
    
    [pendingArray writeToFile:filePath atomically:YES];
}

- (void)removeFromPendingArray {
    
    NSArray *fileArray = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* filePath = [[fileArray objectAtIndex:0] stringByAppendingPathComponent:@"/pending.plist"];
    NSLog(@"File Path: %@", filePath);
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    NSMutableArray *pendingArray;
    if (fileExists) {
        pendingArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    } else {
        // File doesn't exist. Nothing to remove
        return;
    }
    
    if (self.isEditingExistingForm) {
        [pendingArray removeObjectAtIndex:self.editingIndex];
        [pendingArray writeToFile:filePath atomically:YES];
        
        NSLog(@"Pending Array: %@",pendingArray);
    }
    
    
}
                                        
- (NSDictionary *)generateDataDict {
    
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    
    return nil;
}

- (void)populateWithDataDict:(NSDictionary *)dataDict {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}


@end
