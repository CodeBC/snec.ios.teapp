//
//  PastEvaluationViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 6/6/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastEvaluationViewController : UITableViewController

@property (strong, nonatomic) NSArray *doneEvaluations;
@property (strong, nonatomic) NSDictionary *evaluationInfoDict;

@end
