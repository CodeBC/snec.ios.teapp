//
//  UICheckBox.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
  kUICheckBoxStateChecked,
  kUICheckBoxStateUnchecked
} kUICheckBoxState;

@interface UICheckBox : UIButton
{
    kUICheckBoxState checkboxState;
}

- (BOOL)isChecked;
- (void)toggleChecked;
- (void)setChecked:(BOOL)checked;

@end
