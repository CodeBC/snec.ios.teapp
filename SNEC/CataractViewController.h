//
//  CataractViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 23/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FormBaseViewController.h"
#import "UIPlaceHolderTextView.h"

@interface CataractViewController : FormBaseViewController

@property (strong, nonatomic) NSString *traineeName;
@property (strong, nonatomic) NSString *supervisorName;
@property (strong, nonatomic) NSString *eventType;
@property (assign, nonatomic) NSInteger scheduleId;

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UITextField *supervisorTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *numberOfCasesSegmentedControl;
@property (strong, nonatomic) IBOutlet UITextView *strengthsTextView;
@property (strong, nonatomic) IBOutlet UITextView *weaknessTextView;
@property (strong, nonatomic) IBOutlet UIPlaceHolderTextView *complicationsTextView;

@property (strong, nonatomic) IBOutletCollection(UISegmentedControl) NSArray *feedbackSegmentedControls;
@property (strong, nonatomic) IBOutlet UISegmentedControl *surgerySegmentedControl;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIButton *skipButton;

- (IBAction)submitButtonPressed:(id)sender;
- (IBAction)skipButtonPressed:(id)sender;

@end
