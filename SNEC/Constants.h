//
//  Constants.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 20/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClient.h"
#import "SVProgressHUD.h"
#import "NSDate+OrdinalDate.h"

#define BASE_URL @"http://snec-evaluation.herokuapp.com"

#define DEFAULTS_USER_EMAIL_KEY @"email"
#define DEFAULTS_USER_PASSWORD_KEY @"password"
#define DEFAULTS_USER_REMEMBER_KEY @"remember"
#define DEFAULTS_AUTH_TOKEN_KEY @"auth_token"
#define DEFAULTS_USER_TYPE_KEY @"user_type"
#define DEFAULTS_USER_FIRST_NAME_KEY @"first_name"
#define DEFAULTS_USER_LAST_NAME_KEY @"last_name"

#define EVALUATION_TYPE_ID_CATARACT 17
#define EVALUATION_TYPE_ID_GRP 19

typedef enum {
  kUserTypeSupervisor = 4,
  kUserTypeTrainee = 5
} kUserType;

@interface Constants : NSObject

@end
