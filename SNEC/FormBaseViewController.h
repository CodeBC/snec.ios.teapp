//
//  FormBaseViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormBaseViewController : UIViewController <UIAlertViewDelegate>
{
    BOOL completed;
}

@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIToolbar *keyboardToolBar;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) NSDictionary *preloadData;
@property (assign, nonatomic) BOOL isReadOnly;
@property (assign, nonatomic) BOOL isEditingExistingForm;
@property (assign, nonatomic) NSInteger editingIndex;

@property (strong, nonatomic) NSDate *formDate;

- (void)resignKeyboard;

- (void)showDatePicker:(id)sender;
- (void)dismissDatePicker:(id)sender;

- (NSDictionary *)generateDataDict;
- (void)populateWithDataDict:(NSDictionary *)dataDict;

- (IBAction)backButtonPressed:(id)sender;

@end
