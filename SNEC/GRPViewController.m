//
//  GRPViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 23/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "GRPViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface GRPViewController ()

@end

@implementation GRPViewController

@synthesize selectedRow;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) traineeButtonPressed:(id)sender {
    [self displayPicker:nil andTag:999];
}


- (void)removeViews:(id)object {
    [[self.view viewWithTag:19] removeFromSuperview];
    [[self.view viewWithTag:999] removeFromSuperview];
    [[self.view viewWithTag:777] removeFromSuperview];
    [[self.view viewWithTag:111] removeFromSuperview];
}

- (void)dismissPicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:19].alpha = 0;
    [self.view viewWithTag:999].frame = datePickerTargetFrame;
    [self.view viewWithTag:777].frame = datePickerTargetFrame;
    [self.view viewWithTag:111].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

- (IBAction)displayPicker:(id)sender andTag:(int) tag {
    if ([self.view viewWithTag:19]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 19;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    pickerView.tag = tag;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.viewForBaselineLayout.backgroundColor = [UIColor whiteColor];
    pickerView.backgroundColor = [UIColor whiteColor];
    [pickerView selectRow:selectedRow inComponent:0 animated:NO];
    [self.view addSubview:pickerView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 111;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPicker:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    pickerView.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.commentsTextView setInputAccessoryView:super.keyboardToolBar];
    [self.evaluatorTextField setInputAccessoryView:super.keyboardToolBar];
    
    self.commentsTextView.layer.cornerRadius = 5.0f;
    self.commentsTextView.layer.masksToBounds = YES;
    self.commentsTextView.layer.borderWidth = 0.5f;
    self.commentsTextView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    self.residentArray = @[];
    
    [SVProgressHUD showWithStatus:@"Fetching data..." maskType:SVProgressHUDMaskTypeGradient];
    NSString *residentPath = [NSString stringWithFormat:@"/api/v1/users/trainee?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] getPath:residentPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        
        NSError *err;
        /*
         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
         
         if ([responseDict objectForKey:@"data"]) {
         NSLog(@"%@",responseDict);
         self.residentArray = [responseDict objectForKey:@"data"];
         }
         */
        NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseArray) {
            self.residentArray = responseArray;
        }
        
        [SVProgressHUD dismiss];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Error fetching trainees. Please try again."];
    }];
    
    NSString *evaluatorName = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_FIRST_NAME_KEY];
    if (evaluatorName) {
        NSString *name = [NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_FIRST_NAME_KEY], [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_LAST_NAME_KEY]];
        NSLog(@"%@", name);
        [self.evaluatorTextField setText: name];
        [self.evaluatorTextField setEnabled:NO];
    }
    
    
    // Arrange segmented control array by y coordinates
    self.feedbackSegmentedControls = [self.feedbackSegmentedControls sortedArrayUsingComparator:^NSComparisonResult(id view1, id view2) {
        if ([view1 frame].origin.y < [view2 frame].origin.y) return NSOrderedAscending;
        else if ([view1 frame].origin.y > [view2 frame].origin.y) return NSOrderedDescending;
        else return NSOrderedSame;
    }];
    
    // Set Current Date
    self.formDate = [NSDate date];
    [self.dateLabel setText:[self.formDate ordinalString] ];
    
    
    if (self.isReadOnly) {
        [self.contentView setUserInteractionEnabled:NO];
        [self.submitButton setHidden:YES];
    }
    
    if (self.preloadData) {
        [self populateWithDataDict:self.preloadData];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setResidentTextField:nil];
    [self setEvaluatorTextField:nil];
    [self setCommentsTextView:nil];
    [self setFeedbackSegmentedControls:nil];
    [self setDateLabel:nil];
    [self setSubmitButton:nil];
    [super viewDidUnload];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    [self resignKeyboard];
    [self dismissDatePicker:nil];
    
    
    
    if (self.evaluatorTextField.text.length == 0 || self.residentTextField.titleLabel.text.length == 0 || self.commentsTextView.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"Some fields not entered."];
        return;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDictionary *textParams = @{ @"grp[resident]": self.residentTextField.titleLabel.text,
                                  @"grp[evaluator]": self.evaluatorTextField.text,
                                  @"grp[comments]": self.commentsTextView.text,
                                  @"grp[date]": [dateFormatter stringFromDate:self.formDate],
                                  @"grp[schedule_id]": [NSNumber numberWithInt:self.scheduleId] };
    
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] initWithDictionary:textParams];
    
    for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
        
        UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
        NSString *selectedTitle = [NSString stringWithFormat:@"%d",segmentedControl.selectedSegmentIndex+1];
        NSString *paramKey = [NSString stringWithFormat:@"grp[q%d]",i+1];
        [formParams setValue:selectedTitle forKey:paramKey];
        
    }
    
    NSLog(@"Params: %@",formParams);
    
    [SVProgressHUD showWithStatus:@"Submitting form..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSString *feedbackPath = [NSString stringWithFormat:@"/api/v1/grand_round_presentations?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    [[APIClient sharedInstance] postPath:feedbackPath parameters:formParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD showSuccessWithStatus:@"Form Submitted!"];
                completed = YES;
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Form failed to submit. Please try again later."];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Form failed to submit. Please try again later."];
            NSLog(@"%@",[err localizedDescription]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Submission Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
    
}

#pragma mark - Keyboard Handlers

- (void)handleKeyboardNotification:(NSNotification*)aNotification directionUp:(BOOL)up {
    
    NSDictionary* userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    CGRect newFrame = self.scrollView.frame;
    newFrame.size.height -= (keyboardFrame.size.height) * (up ? 1 : -1);
    [self.scrollView setFrame:newFrame];
    
    [UIView commitAnimations];
}

- (void)resignKeyboard {
    [self.residentTextField resignFirstResponder];
    [self.evaluatorTextField resignFirstResponder];
    [self.commentsTextView resignFirstResponder];
}

#pragma mark Pending Methods

- (NSDictionary *)generateDataDict {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    NSDictionary *textParams = @{ @"form_name": @"GRP Form",
                                  @"resident": self.residentTextField.titleLabel.text,
                                  @"evaluator": self.evaluatorTextField.text,
                                  @"comments": self.commentsTextView.text,
                                  @"date": [dateFormatter stringFromDate:self.formDate],
                                  @"schedule_id": [NSNumber numberWithInt:self.scheduleId] };
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithDictionary:textParams];
    
    for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
        
        UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
        NSNumber *selectedTitle = [NSNumber numberWithInt:segmentedControl.selectedSegmentIndex+1];
        NSString *paramKey = [NSString stringWithFormat:@"q%d",i+1];
        [dataDict setValue:selectedTitle forKey:paramKey];
        
    }
    
    return dataDict;
}

- (void)populateWithDataDict:(NSDictionary *)dataDict {
    
    @try {
        self.scheduleId = [[dataDict objectForKey:@"schedule_id"] intValue];
        self.residentTextField.titleLabel.text = [dataDict objectForKey:@"resident"];
        self.evaluatorTextField.text = [dataDict objectForKey:@"evaluator"];
        self.commentsTextView.text = [dataDict objectForKey:@"comments"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        self.formDate = [dateFormatter dateFromString:[dataDict objectForKey:@"date"]];
        self.dateLabel.text = [self.formDate ordinalString];
        
        for (NSInteger i=0; i<[self.feedbackSegmentedControls count]; i++) {
            
            NSString *paramKey = [NSString stringWithFormat:@"q%d",i+1];
            
            UISegmentedControl *segmentedControl = [self.feedbackSegmentedControls objectAtIndex:i];
            [segmentedControl setSelectedSegmentIndex:[[dataDict objectForKey:paramKey] intValue]-1];
            
        }
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Caught: %@",exception);
        [SVProgressHUD showErrorWithStatus:@"Form has errors"];
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.residentArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@ %@",[[self.residentArray objectAtIndex:row] objectForKey:@"first_name"],[[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (thePickerView.tag == 999) {
        NSString *title = [NSString stringWithFormat:@"%@ %@", [[self.residentArray objectAtIndex:row] objectForKey:@"first_name"], [[self.residentArray objectAtIndex:row] objectForKey:@"last_name"]];
        [self.residentTextField setTitle:title forState:UIControlStateNormal];
    }
    
}


@end
