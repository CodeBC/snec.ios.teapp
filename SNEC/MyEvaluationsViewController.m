//
//  MyEvaluationsViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 28/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "MyEvaluationsViewController.h"
#import "FormBaseViewController.h"

@interface MyEvaluationsViewController ()

@property (nonatomic, strong) NSMutableArray *pendingArray;

@property (nonatomic, strong) NSArray *praiseArray;
@property (nonatomic, strong) NSArray *cataractArray;
@property (nonatomic, strong) NSArray *feedbackArray;
@property (nonatomic, strong) NSArray *concernArray;
@property (nonatomic, strong) NSArray *grpArray;

@end

@implementation MyEvaluationsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.pendingArray = [NSMutableArray new];
    
    self.praiseArray = @[];
    self.concernArray = @[];
    self.feedbackArray = @[];
    self.cataractArray = @[];
    self.grpArray = @[];
    
    [self fetchSubmittedData];
}

- (void)viewDidAppear:(BOOL)animated {
    [self refreshPendingData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self savePendingArray];
}

- (void)savePendingArray {
    
    NSArray *fileArray = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* filePath = [[fileArray objectAtIndex:0] stringByAppendingPathComponent:@"/pending.plist"];
    NSLog(@"File Path: %@", filePath);
    
    [self.pendingArray writeToFile:filePath atomically:YES];
}

- (void)refreshPendingData {
    
    NSArray *fileArray = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* filePath = [[fileArray objectAtIndex:0] stringByAppendingPathComponent:@"/pending.plist"];
    NSLog(@"File Path: %@", filePath);
    
    NSMutableArray *loadedArray = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    
    if (loadedArray) {
        NSLog(@"%@",loadedArray);
        self.pendingArray = loadedArray;
        [self.tableView reloadData];
    }
    
}

- (void)fetchSubmittedData {
    NSString *evaluationsPath = [NSString stringWithFormat:@"/api/v1/evaluations?auth_token=%@",[[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY] ];
    
    [SVProgressHUD showWithStatus:@"Loading submitted forms..."];
    [[APIClient sharedInstance] getPath:evaluationsPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD dismiss];
                
                self.praiseArray = [responseDict objectForKey:@"praise_cards"];
                self.concernArray = [responseDict objectForKey:@"early_concern_cards"];
                self.feedbackArray = [responseDict objectForKey:@"feedbacks"];
                self.cataractArray = [responseDict objectForKey:@"cataract_surgeries"];
                self.grpArray = [responseDict objectForKey:@"grand_round_presentations"];
                
                [self.tableView reloadData];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"Error getting data"];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"Error getting data"];
            NSLog(@"JSON Error %@",[err localizedDescription]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Evaluations Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)refreshButtonPressed:(id)sender {
    [self fetchSubmittedData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 6;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"Pending Forms";
    } else if (section == 1) {
        return @"Submitted Cataract Forms";
    } else if (section == 2) {
        return @"Submitted GWR Forms";
    } else if (section == 3) {
        return @"Submitted Early Concern Card Forms";
    } else if (section == 4) {
        return @"Submitted Praise Card Forms";
    } else if (section == 5) {
        return @"Submitted Faculty Feedback";
    } else {
        return @"";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if (section == 0) {
        return [self.pendingArray count];
    } else if (section == 1) {
        return [self.cataractArray count];
    } else if (section == 2) {
        return [self.grpArray count];
    } else if (section == 3) {
        return [self.concernArray count];
    } else if (section == 4) {
        return [self.praiseArray count];
    } else if (section == 5) {
        return [self.feedbackArray count];
    } else {
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"EvaluationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter3 setTimeZone:[NSTimeZone localTimeZone]];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd MMM yyyy"];

    
    if (indexPath.section == 0) {
        NSString *dateString = [[self.pendingArray objectAtIndex:indexPath.row] objectForKey:@"date"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];

        cell.textLabel.text = [[[self.pendingArray objectAtIndex:indexPath.row] objectForKey:@"form_name"] stringByAppendingString:convertedString];
    } else if (indexPath.section == 1) {
        NSString *dateString = [[self.cataractArray objectAtIndex:indexPath.row] objectForKey:@"date"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text = [[[self.cataractArray objectAtIndex:indexPath.row] objectForKey:@"trainee"] stringByAppendingFormat:@" (%@)", convertedString];
    } else if (indexPath.section == 2) {
        NSString *dateString = [[self.grpArray objectAtIndex:indexPath.row] objectForKey:@"date"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text = [[[self.grpArray objectAtIndex:indexPath.row] objectForKey:@"resident"] stringByAppendingFormat:@" (%@)", convertedString];
    } else if (indexPath.section == 3) {
        NSString *dateString = [[self.concernArray objectAtIndex:indexPath.row] objectForKey:@"doa"];
        NSDate *date = [dateFormatter3 dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text = [[[self.concernArray objectAtIndex:indexPath.row] objectForKey:@"resident"]  stringByAppendingFormat:@" (%@)", convertedString];
    } else if (indexPath.section == 4) {
        NSString *dateString = [[self.praiseArray objectAtIndex:indexPath.row] objectForKey:@"doa"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text = [[[self.praiseArray objectAtIndex:indexPath.row] objectForKey:@"resident"]  stringByAppendingFormat:@" (%@)", convertedString];
    } else if (indexPath.section == 5) {
        NSString *dateString = [[self.feedbackArray objectAtIndex:indexPath.row] objectForKey:@"date_of_activity"];
        NSDate *date = [dateFormatter3 dateFromString:dateString];
        NSString *convertedString = [dateFormatter2 stringFromDate:date];
        
        cell.textLabel.text = [[[self.feedbackArray objectAtIndex:indexPath.row] objectForKey:@"trainee_name"]  stringByAppendingFormat:@" (%@)", convertedString];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 0) {
        return YES;
    } else {
        return NO;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.pendingArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    FormBaseViewController *formView;
    
    if (indexPath.section == 0) {
        
        NSString *formName = [[self.pendingArray objectAtIndex:indexPath.row] objectForKey:@"form_name"];
        
        if ([formName isEqualToString:@"Early Concern Card"]) {
            
            formView = [self.storyboard instantiateViewControllerWithIdentifier:@"EarlyConcernForm"];
            
        } else if ([formName isEqualToString:@"Faculty Feedback Form"]) {
            
            formView = [self.storyboard instantiateViewControllerWithIdentifier:@"FacultyFeedbackForm"];
            
        } else if ([formName isEqualToString:@"Praise Card Form"]) {
            
            formView = [self.storyboard instantiateViewControllerWithIdentifier:@"PraiseCardForm"];
            
        } else if ([formName isEqualToString:@"Cataract Surgery Form"]) {
            
            formView = [self.storyboard instantiateViewControllerWithIdentifier:@"CataractForm"];
            
        } else if ([formName isEqualToString:@"GRP Form"]) {
            
            formView = [self.storyboard instantiateViewControllerWithIdentifier:@"GRPView"];
            
        }
        
        if (formView) {
            formView.preloadData = [self.pendingArray objectAtIndex:indexPath.row];
            formView.isEditingExistingForm = YES;
            formView.editingIndex = indexPath.row;
        }
    
    } else if (indexPath.section == 1) {
        
        formView = [self.storyboard instantiateViewControllerWithIdentifier:@"CataractForm"];
        formView.preloadData = [self.cataractArray objectAtIndex:indexPath.row];
        formView.isReadOnly = YES;
        
    } else if (indexPath.section == 2) {
        
        formView = [self.storyboard instantiateViewControllerWithIdentifier:@"GRPView"];
        formView.preloadData = [self.grpArray objectAtIndex:indexPath.row];
        formView.isReadOnly = YES;
        
    } else if (indexPath.section == 3) {
        
        formView = [self.storyboard instantiateViewControllerWithIdentifier:@"EarlyConcernForm"];
        formView.preloadData = [self.concernArray objectAtIndex:indexPath.row];
        formView.isReadOnly = YES;
        
    } else if (indexPath.section == 4) {
        
        formView = [self.storyboard instantiateViewControllerWithIdentifier:@"PraiseCardForm"];
        formView.preloadData = [self.praiseArray objectAtIndex:indexPath.row];
        formView.isReadOnly = YES;
        
    } else if (indexPath.section == 5) {
        
        formView = [self.storyboard instantiateViewControllerWithIdentifier:@"FacultyFeedbackForm"];
        formView.preloadData = [self.feedbackArray objectAtIndex:indexPath.row];
        formView.isReadOnly = YES;
        
    }
    
    if (formView) {
        [self.navigationController pushViewController:formView animated:YES];
    }
}


@end
