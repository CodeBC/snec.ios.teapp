//
//  EvaluationInfoViewController.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 19/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "EvaluationInfoViewController.h"
#import "CataractViewController.h"
#import "GRPViewController.h"
#import "PastEvaluationViewController.h"

@interface EvaluationInfoViewController ()

@end

@implementation EvaluationInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    if (self.evaluationInfoDict) {
        NSLog(@"Evaluation Info Dict: %@",self.evaluationInfoDict);
        self.title = [self.evaluationInfoDict objectForKey:@"title"];
        self.titleLabel.text = [self.evaluationInfoDict objectForKey:@"evaluation_type_name"];
        self.dateLabel.text = [self.evaluationInfoDict objectForKey:@"date"];
        self.locationLabel.text = [self.evaluationInfoDict objectForKey:@"location_name"];
        
        NSDictionary *supervisorDict = [self.evaluationInfoDict[@"supervisors"] objectAtIndex:0];
        NSString *supervisorString = [NSString stringWithFormat:@"%@ %@",[supervisorDict objectForKey:@"first_name"],[supervisorDict  objectForKey:@"last_name"] ];
        
        NSDictionary *traineeDict = [self.evaluationInfoDict[@"trainees"] objectAtIndex:0];
        NSString *traineeString = [NSString stringWithFormat:@"%@ %@",traineeDict[@"first_name"],traineeDict[@"last_name"] ];
        
        [self.supervisorButton setTitle:supervisorString forState:UIControlStateNormal];
        [self.traineeButton setTitle:traineeString forState:UIControlStateNormal];
        
        if ([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_CATARACT) {
            
            [self.supervisorButton setEnabled:NO];
            
            if (![[self.evaluationInfoDict objectForKey:@"has_no_latest_evaluation"] boolValue]) {
                [self.pastEvaluationButton setHidden:NO];
            }
            
            self.scheduleTypeLabel.text = [self.evaluationInfoDict objectForKey:@"schedule_type_name"];
        }
        
        if ([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_GRP) {
            [self.supervisorHeaderLabel setText:@"Evaluator:"];
            [self.traineeHeaderLabel setText:@"Resident:"];
            
            [self.scheduleTypeLabel setHidden:YES];
            [self.scheduleTypeHeaderLabel setHidden:YES];
        }
        
        if ([[self.evaluationInfoDict objectForKey:@"has_done_evaluation"] boolValue]) {
            [self.evaluationButton setHidden:YES];
        }

        if([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_GRP ) {
            [self.pastEvaluationButton setHidden:YES];
            if ([[self.evaluationInfoDict objectForKey:@"has_done_evaluation"] boolValue]) {
                [self.traineeButton setEnabled:NO];
            } else {
                [self.traineeButton setEnabled:YES];
            }
        }
        
        
        
        selectedRow = 0;

    } // End If self.evaluationDict
}


- (IBAction)traineeButtonPressed:(id)sender {
    NSLog(@"Clicking trainee button");
    [self displayPicker:nil];
}


#pragma mark - Picker Data/Delegate

- (void)removeViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissPicker:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

- (IBAction)displayPicker:(id)sender {
    if ([self.view viewWithTag:9]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    pickerView.tag = 10;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    [pickerView selectRow:selectedRow inComponent:0 animated:NO];
    [self.view addSubview:pickerView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissPicker:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    pickerView.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (self.evaluationInfoDict) {
        
        NSNumber *userType = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_TYPE_KEY];
        
        switch ([userType intValue]) {
            case kUserTypeSupervisor:
                return [[self.evaluationInfoDict objectForKey:@"trainees"] count];
                break;
                
            case kUserTypeTrainee:
                return [[self.evaluationInfoDict objectForKey:@"supervisors"] count];
                break;
                
            default:
                return 0;
                break;
        }
        
    } else {
        return 0;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (self.evaluationInfoDict) {
        
        NSNumber *userType = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_TYPE_KEY];
        
        switch ([userType intValue]) {
            case kUserTypeSupervisor:
                return [[[self.evaluationInfoDict objectForKey:@"trainees"] objectAtIndex:row] objectForKey:@"first_name"];
                break;
                
            case kUserTypeTrainee:
                return [[[self.evaluationInfoDict objectForKey:@"supervisors"] objectAtIndex:row] objectForKey:@"first_name"];
                break;
                
            default:
                return nil;
                break;
        }
        
    } else {
        return nil;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (self.evaluationInfoDict) {
        
        NSNumber *userType = [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_USER_TYPE_KEY];
        
        switch ([userType intValue]) {
            case kUserTypeSupervisor:
                [self.traineeButton setTitle:[[[self.evaluationInfoDict objectForKey:@"trainees"] objectAtIndex:row] objectForKey:@"first_name"] forState:UIControlStateNormal];
                break;
                
            case kUserTypeTrainee:
                [self.supervisorButton setTitle:[[[self.evaluationInfoDict objectForKey:@"supervisors"] objectAtIndex:row] objectForKey:@"first_name"] forState:UIControlStateNormal];
                break;
        }
        
    }
    
    selectedRow = row;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitleLabel:nil];
    [self setDateLabel:nil];
    [self setLocationLabel:nil];
    [self setSupervisorLabel:nil];
    [self setTraineeLabel:nil];
    [self setEvaluationButton:nil];
    [self setSupervisorButton:nil];
    [self setTraineeButton:nil];
    [self setPastEvaluationButton:nil];
    [self setSupervisorHeaderLabel:nil];
    [self setTraineeHeaderLabel:nil];
    [self setScheduleTypeLabel:nil];
    [self setScheduleTypeHeaderLabel:nil];
    [super viewDidUnload];
}

- (IBAction)evaluationButtonPressed:(id)sender {
    
    NSLog(@"%@", self.evaluationInfoDict);
    
    if (self.evaluationInfoDict) {
        if ([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_CATARACT) {
            
            CataractViewController *cataractView = [self.storyboard instantiateViewControllerWithIdentifier:@"CataractForm"];
            cataractView.traineeName = [self.traineeButton titleForState:UIControlStateNormal];
            cataractView.supervisorName = [self.supervisorButton titleForState:UIControlStateNormal];
            cataractView.scheduleId= [[self.evaluationInfoDict objectForKey:@"id"] intValue];
            cataractView.eventType = [self.evaluationInfoDict objectForKey:@"schedule_type_name"];
            [self.navigationController pushViewController:cataractView animated:YES];
            
        } else if ([[self.evaluationInfoDict objectForKey:@"evaluation_type_id"] intValue] == EVALUATION_TYPE_ID_GRP) {
            
            GRPViewController *grpView = [self.storyboard instantiateViewControllerWithIdentifier:@"GRPView"];
            grpView.traineeName = [self.traineeButton titleForState:UIControlStateNormal];
            grpView.supervisorName = [self.supervisorButton titleForState:UIControlStateNormal];
            grpView.scheduleId = [[self.evaluationInfoDict objectForKey:@"id"] intValue];
            [self.navigationController pushViewController:grpView animated:YES];
            
        }
    }
}

- (IBAction)pastEvaluationButtonPressed:(id)sender {
    
    PastEvaluationViewController *pastEvaluationView = [self.storyboard instantiateViewControllerWithIdentifier:@"PastEvaluationView"];
    pastEvaluationView.evaluationInfoDict = self.evaluationInfoDict;
    
    NSSortDescriptor * sortByDate =
    [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *doneEvaluations = [[[self.evaluationInfoDict objectForKey:@"trainees"] objectAtIndex:0] objectForKey:@"cataract_surgeries"];
    NSArray *sortDescriptors = @[sortByDate];
    NSArray *sortedArray = [doneEvaluations sortedArrayUsingDescriptors:sortDescriptors];

    pastEvaluationView.doneEvaluations = sortedArray;
    
    [self.navigationController pushViewController:pastEvaluationView animated:YES];
    
}

@end
