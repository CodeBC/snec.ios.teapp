//
//  MyEvaluationsViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 28/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEvaluationsViewController : UITableViewController

- (IBAction)refreshButtonPressed:(id)sender;

@end
