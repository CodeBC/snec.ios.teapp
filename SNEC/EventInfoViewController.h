//
//  EventInfoViewController.h
//  SNEC
//
//  Created by Ye Myat Min on 7/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventInfoViewController : QuickDialogController

@property (strong, nonatomic) NSDictionary *eventInfoDict;
+ (NSString *)stringByStrippingHTML:(NSString *)inputString;

@end
