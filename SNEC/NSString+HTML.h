//
//  NSString+HTML.h
//  SNEC
//
//  Created by Ye Myat Min on 8/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)
- (NSString*) stringByStrippingHTML;

@end
