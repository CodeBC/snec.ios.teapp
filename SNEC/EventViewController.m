//
//  EventViewController.m
//  SNEC
//
//  Created by Ye Myat Min on 7/8/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "EventViewController.h"
#import "EventInfoViewController.h"

@interface EventViewController ()

@property (nonatomic, strong) NSArray *eventsArray;
@property (nonatomic,strong) NSMutableArray *dateMarkerArray;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@end


@implementation EventViewController

#pragma mark View Lifecycle
- (void) viewDidLoad {
	[super viewDidLoad];
    
	[self.monthView selectDate:[NSDate date]];
    self.dateMarkerArray = [NSMutableArray array];
    [self loadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshButtonPressed:)
                                                 name:@"REFRESH_SCHEDULE" object:nil];
}

- (void)loadData {
    NSDictionary *scheduleParams = @{ @"auth_token": [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULTS_AUTH_TOKEN_KEY]};
    
    NSLog(@"%@", scheduleParams);
    
    [SVProgressHUD showWithStatus:@"Retrieving events..." maskType:SVProgressHUDMaskTypeGradient];
    [[APIClient sharedInstance] getPath:@"/api/v1/events" parameters:scheduleParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",responseString);
        
        NSError *err;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        
        if (responseDict) {
            NSLog(@"%@",responseDict);
            
            if ([[responseDict objectForKey:@"status"] intValue] == 1) {
                [SVProgressHUD dismiss];
                
                self.eventsArray = [responseDict objectForKey:@"events"];
                [self.monthView reloadData];
                [self.tableView reloadData];
                
                
            } else {
                [SVProgressHUD showErrorWithStatus:[responseDict objectForKey:@"message"]];
            }
            
        } else {
            [SVProgressHUD showErrorWithStatus:[responseDict objectForKey:@"message"]];
            NSLog(@"%@",[err localizedDescription]);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Submission Error: %@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Unable to connect to server."];
    }];
    
}

#pragma mark MonthView Delegate & DataSource
- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate{
    [self generateMarkerArrayForStartDate:startDate endDate:lastDate];
	return self.dateMarkerArray;
}
- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date{
	NSLog(@"Date Selected: %@",date);
	[self.tableView reloadData];
}
- (void) calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d animated:(BOOL)animated{
	[super calendarMonthView:mv monthDidChange:d animated:animated];
    [mv reloadData];
	[self.tableView reloadData];
}


#pragma mark UITableView Delegate & DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSArray *eventsOnDate = self.dataDictionary[[self.monthView dateSelected]];
	if(eventsOnDate == nil) return 0;
	return [eventsOnDate count];
}

- (UITableViewCell *) tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"EventCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
	NSArray *eventsOnDate = self.dataDictionary[[self.monthView dateSelected]];
	cell.textLabel.text = [eventsOnDate[indexPath.row] objectForKey:@"title"];
    
    NSString *detailString = [NSString stringWithFormat:@"%@",[eventsOnDate[indexPath.row] objectForKey:@"start_at"]];
    cell.detailTextLabel.text = detailString;
	
    return cell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EventInfoViewController *eventView = [self.storyboard instantiateViewControllerWithIdentifier:@"EventInfoView"];
    
    NSArray *eventsOnDate = self.dataDictionary[[self.monthView dateSelected]];
    eventView.eventInfoDict = eventsOnDate[indexPath.row];
    [self.navigationController pushViewController:eventView animated:YES];
    
}


#pragma mark - Helper Functions

- (void)generateMarkerArrayForStartDate:(NSDate *)start endDate:(NSDate *)end {
    NSLog(@"Delegate Range: %@ %@ %d",start,end,[start daysBetweenDate:end]);
    
    self.dateMarkerArray = [NSMutableArray array];
	self.dataDictionary = [NSMutableDictionary dictionary];
    
    // Generate Default
    NSInteger totalDays = [start daysBetweenDate:end];
    for (int i=0; i<totalDays; i++) {
        [self.dateMarkerArray addObject:@NO];
    }
    
    for (NSDictionary *eventInfo in self.eventsArray) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm a"];
        [dateFormatter setLocale:usLocale];
        
        // Parse Date
        NSDate *eventDateTime = [dateFormatter dateFromString:[eventInfo objectForKey:@"start_at"]];
        NSLog(@"Start at %@", [eventInfo objectForKey:@"start_at"]);
        
        
        if ([[eventDateTime earlierDate:start] isEqualToDate:eventDateTime]) {
            // Event is earlier than start
            continue;
        }
        
        if ([[eventDateTime laterDate:end] isEqualToDate:eventDateTime]) {
            // Event is later than end
            continue;
        }
        
        // Remove time component
        NSDateComponents *info = [eventDateTime dateComponentsWithTimeZone:self.monthView.timeZone];
        info.hour = 0;
        info.minute = 0;
        info.second = 0;
        NSDate *eventDateOnly = [NSDate dateWithDateComponents:info];

        NSLog(@"Time zone %@", self.monthView.timeZone);
        NSLog(@"Event Date Time %@", eventDateTime);
        NSLog(@"Info %@", info);
        NSLog(@"Event Date %@", eventDateOnly);

        // Event Array already exist on same date
        if ([self.dataDictionary objectForKey:eventDateOnly]) {
            // Add eventInfo to existing array
            NSMutableArray *dateEvents = [self.dataDictionary objectForKey:eventDateOnly];
            [dateEvents addObject:eventInfo];
        } else { // First event for the date
            // Create new array
            [self.dateMarkerArray replaceObjectAtIndex:[start daysBetweenDate:eventDateOnly] withObject:@YES];
            NSMutableArray *dateEvents = [[NSMutableArray alloc] init];
            [dateEvents addObject:eventInfo];
            [self.dataDictionary setObject:dateEvents forKey:eventDateOnly];
        }

    }
    
}

- (IBAction)refreshButtonPressed:(id)sender {
    [self loadData];
}

@end
