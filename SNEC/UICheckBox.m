//
//  UICheckBox.m
//  SNEC
//
//  Created by Nur Iman Izam Othman on 22/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "UICheckBox.h"

@implementation UICheckBox

- (void)awakeFromNib {
    checkboxState = kUICheckBoxStateUnchecked;
    [self addTarget:self action:@selector(toggleChecked) forControlEvents:UIControlEventTouchUpInside];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        checkboxState = kUICheckBoxStateUnchecked;
        [self addTarget:self action:@selector(toggleChecked) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)isChecked {
    return (checkboxState == kUICheckBoxStateChecked);
}

- (void)toggleChecked {
    if (checkboxState == kUICheckBoxStateChecked) {
        [self setChecked:NO];
    } else {
        [self setChecked:YES];
    }
}

- (void)setChecked:(BOOL)checked {
    if (!checked) {
        [self setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        checkboxState = kUICheckBoxStateUnchecked;
    } else {
        [self setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        checkboxState = kUICheckBoxStateChecked;
    }
}

@end
