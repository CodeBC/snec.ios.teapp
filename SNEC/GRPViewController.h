//
//  GRPViewController.h
//  SNEC
//
//  Created by Nur Iman Izam Othman on 23/5/13.
//  Copyright (c) 2013 Nur Iman Izam Othman. All rights reserved.
//

#import "FormBaseViewController.h"

@interface GRPViewController : FormBaseViewController <UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) NSString *traineeName;
@property (strong, nonatomic) NSString *supervisorName;
@property (assign, nonatomic) NSInteger scheduleId;

@property (strong, nonatomic) NSArray *residentArray;
@property (strong, nonatomic) NSArray *facultyArray;
@property (assign, nonatomic) NSInteger selectedRow;


@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIButton *residentTextField;
@property (strong, nonatomic) IBOutlet UITextField *evaluatorTextField;
@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;

@property (strong, nonatomic) IBOutletCollection(UISegmentedControl) NSArray *feedbackSegmentedControls;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)submitButtonPressed:(id)sender;
- (IBAction)traineeButtonPressed:(id)sender;


@end
